#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <iostream>
#include <string.h>
using namespace std;

struct node{
	string data;
	node* next;
};

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
/*
struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
*/
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */


void create_list(node*& head_ptr);
void print(node* head_ptr);
void sort(node*& head_ptr);
void reverse(node*&  head_ptr);
void remove_duplicate(node*& head_ptr);
void ignore_sort(node*& head_ptr);


int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* TODO: write me... */

	node* head_ptr = NULL;
	create_list(head_ptr);
//	cout << "First let's print the lines.\n";
//	print(head_ptr);

//	cout << "i'll remove the duplicate\n";
	if (unique == 1){
		remove_duplicate(head_ptr);
	}


//	cout << "lets ignore the case\n";
	if (ignorecase == 1){
		ignore_sort(head_ptr);
	}else{
		sort(head_ptr);
	}


//	cout << "Lets print it in reverse\n";
	if (descending == 1){
		reverse(head_ptr);
	}

	print(head_ptr);
	return 0;
}


void create_list(node*& head_ptr){
	string s;
	node* tail_ptr = NULL;
	while (getline(cin,s)){
		node* n = new node;
		n->data = s;
		n->next = NULL;
		if ( head_ptr == NULL){
			head_ptr = n;
			tail_ptr = n;
		}else{
			tail_ptr->next = n;
			tail_ptr = n;
		}
	}
}


void print(node* head_ptr){
	node* i = head_ptr;
	while( i != NULL){
		cout << i->data <<"\n";
		i = i->next;
	}
}


void sort(node*& head_ptr){
	node* smallest = NULL;
	for (node* i = head_ptr; i != NULL; i = i->next){
		smallest = i;
		for (node*j = i->next; j != NULL; j = j->next){
			if (j->data < smallest->data){
				smallest = j;
			}
		}
		swap(i->data, smallest->data);
	}
}


void reverse(node*&  head_ptr){
	node* previous = NULL;
	node* current = head_ptr;
	node* follow = NULL;
	while (current != NULL){
		follow = current->next;
		current->next = previous;
		previous = current;
		current = follow;
		if( current == NULL)
			head_ptr = previous;
	}
}


void remove_duplicate(node*& head_ptr){
	for (node* i = head_ptr; i != NULL; i = i->next){
			node* previous = i;
		for(node* j = i->next; j != NULL; j = j->next){
			if (j->data == i->data){
				node* follow = j->next;
				previous->next = follow;
				delete j;
				j = previous;
			}else{
				previous = previous->next;
			}
		}
	}
}


void ignore_sort(node*& head_ptr){
	node* smallest = NULL;
	for (node* i = head_ptr; i != NULL; i = i->next){
		smallest = i;
		for (node*j = i->next; j != NULL; j = j->next){
			if ( strcasecmp( (j->data).c_str(), (smallest->data).c_str() ) <= 0 ){
				smallest = j;
			}
		}
		swap(i->data, smallest->data);
	}
}


// passed all tests :D
