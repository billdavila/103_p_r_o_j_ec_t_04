#include <iostream>
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

	enum STATES {ws, nws}; //Has to be in the front for it to work

	size_t countBytes(size_t &bytes){
		bytes++;
		return bytes;
	}

	size_t countLines(size_t &lines, const char ch){
		if (ch == '\n')
			lines++;
		return lines;
	}

	size_t countWords(size_t &words, STATES &state, const char ch){
		if (state == nws){
			if (ch == ' ' || ch == '\t' || ch == '\n'){
				words++;
			}
		}
		return words;
	}

	set<string> uniqueWordMap (set<string> uniqueWords, string &str, STATES &state, const char ch){
		if (state == nws){
			if (ch == ' ' || ch == '\t' || ch == '\n'){
				uniqueWords.insert(str);		//Insert a word, set only store unique words
				str.clear();								//Erase the current string, ready to store next one
				state = ws;
			}
			else
				str+=ch;
		}
		else {
			if (ch != ' ' && ch != '\t' && ch != '\n'){
				str+=ch;
				state = nws;
			}
		}
		return uniqueWords;
	}

	size_t longLength(size_t &maxLength, size_t &length, const char ch){
		if (ch == '\n'){
			if (length > maxLength){
				maxLength = length;
			}
			length = 0;
		}
		else if (ch == '\t')
			length += 8-length%8;
		else
			length++;
		return maxLength;
	}

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	STATES state;
	char ch;
	size_t bytes = 0;
	size_t lines = 0;
	size_t words = 0;
	size_t length = 0;        //to hold each line
	size_t maxLength = 0;			//comparison to find the longer line
	string str;               //to hold the WHOLE text*
	fread(&ch, 1, 1, stdin);  //to determine the first ch
	if (ch == ' '){
		state = ws;
		bytes++;
		str += ch;
		length++;
	}
	else if (ch == '\t'){
		state = ws;
		bytes++;							//\t also take only 1 byte
		length += 8-length%8;
	}
	else if (ch == '\n'){
		state = ws;
		bytes++;
		lines++;
	}
	else{
		state = nws;
		bytes++;
		str += ch;
		length++;
	}
	set <string> uniqueWords;
	while (fread(&ch, 1, 1, stdin)){
		countBytes(bytes);
		countLines(lines, ch);
		countWords(words, state, ch);
		uniqueWordMap(uniqueWords, str, state, ch);
		longLength(maxLength, length, ch);
	}
	//Use the EOF to test the end of the stdin.

	if (feof(stdin)){
		if (state == nws){
			uniqueWords.insert(str);
			words++;
		}
	}

	if (linesonly == 1)
		//cout << '\t' << lines;
	printf("%7lu ", lines);
	if (wordsonly == 1)
		//cout << '\t' << words;
	printf("%7lu ", words);
	if (charonly == 1)
		//cout << '\t' << bytes;
	printf("%7lu ", bytes);
	if (uwordsonly == 1){
		size_t total = 0;
		for (set<string>::iterator i = uniqueWords.begin(); i != uniqueWords.end(); i++){
			total++;
		}
		//cout << '\t' << total;
		printf("%7lu ", total);
	}
	if (longonly == 1)
		//cout << '\t' << maxLength;
	printf("%7lu ", maxLength);
	if (linesonly != 1 && wordsonly != 1 && charonly != 1 && uwordsonly != 1 && longonly != 1)
		//cout << '\t' << lines << '\t' << words << '\t' << bytes;
	printf("%7lu ", lines);
	printf("%7lu ", words);
	printf("%7lu ", bytes);

	cout << "\n";
	return 0;
}