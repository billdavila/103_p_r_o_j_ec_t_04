/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
              https://stackoverflow.com/questions/8067338/vector-of-structs-initialization
*            classmate:  Ziyi Huang, Ai Hua Li
 * Finally, please indicate approximately how many hours you spent on this:
 #hours:
 */

#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";



//struct that handles different types for a vector
struct vectype
{
			string vec_content ;
			size_t vec_index  ;
};


	/*
	The purpose of this function is to convert
	a vector of character into a string
	There are multiple way to do it but I think
	this is the most simple one.
	*/
void characters_to_str(vector<char>& A, string& B)
{
						B = ""; //set B to empty string
				for(size_t i = 0; i < A.size() ; ++i)
				{
						B = B + A[i]; //put every element of array A into string B;
				}
}

int main(int argc, char *argv[])
{
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count = -1 ;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */

	//a vector of structure that handles the target input of different type
	  vector<vectype>  shuffle ;


  /*
	When the user provides the -e option, we stors all the
	strings given , shuffle them and them print them. If the
	user also provides a -n option then we need to show certain number
	of lines. That condition needs to be check at the end so we don't
  get a segmentation fault. Also, notice that we don't need another
  condition since -e and -i are mutually exclusive. */
					if(echo == 1)
					{
											size_t myindex = 0;
											// non-option argument
						          // notice that the optind variable is
						          // supplied in the '<getopt.h>' header
											while (optind < argc)
											{
													shuffle.push_back(vectype());
													shuffle[myindex].vec_content = argv[optind];
												  ++myindex ;
												  ++optind ;
											}

											size_t randnum = 0; //store the random number
											srand(time(0)); //use the current time to determine pseudorandom sequence

											/*
											Once we have a random number depending on the size of the vector
											we store it in a variable randnum which is a random selected index from the
											vector. After that we just swap it with the current element of the loop which
											is i. It's very similar to the naive sorting algorithm but instead of
											choosing the smallest one, we just pick a random one.
											*/
											for(size_t i = 0; i < shuffle.size(); ++i)
											{
												   randnum =  ( rand() % (shuffle.size()-i) ) + i  ;
													 swap( shuffle[i].vec_content,shuffle[randnum].vec_content);
											}

											/*	After that we just print out the shuffled vector
                      but we need to check if the user provide  -n option
										 we need to be careful with size. Otherwise we can get
										 a segmentation fault.
											*/

												if( count < shuffle.size() )
												{
																	for(size_t i = 0; i < count ; ++i)
																	{
																		cout << shuffle[i].vec_content << "\n";
																	}
												}
												else
												{
																	for(size_t i = 0; i < shuffle.size(); ++i)
																	{
																			cout << shuffle[i].vec_content << "\n";
																	}
												}
					}

	/*
	When the user provides the -i option first
	we need to store them from rlow to rhigh. Do the shuffle
	and then print. Again we need to be careful if the -n option
	was also given. We don't need to worry about -e. Since they
	are mutually exclusive.
	*/
					else if(userange == true)
					{
													size_t myindex = 0;
						            /* First we need to store them from rlow to rhigh
						             in our shuffle vector*/

													for(int i = rlow; i <= rhigh; ++i)
						              {
															shuffle.push_back(vectype());
															shuffle[myindex].vec_index = i;
															++myindex ;
													}

													/*Again we need to do the shuffling. We do the same
													strategy as before.Once we have a random number
													depending on the size of the vector we store it in
													a variable randnum which is a random selected index from the
											    vector. After that we just swap it with the current
													element of the loop which is i. It's very similar
													to the naive sorting algorithm but instead of choosing
													the smallest one, we just pick a random one.
													*/

													size_t randnum = 0;
													srand(time(0));
													for(size_t i = 0; i < shuffle.size(); ++i)
													{
																randnum =  ( rand() % (shuffle.size() -i )  + i )        ;
																swap(shuffle[i].vec_index,shuffle[randnum].vec_index);
													}


													/* Once, the vector is shuffled. We just need to print it
													but again we need to check if the user did not provide the -n
													with a number that we can't do the printing. Otherwise we will
													get a segmentation error.
													*/

															if( count < shuffle.size() )
															{
																		for( size_t i = 0; i < count ; ++i)
																		{
																				cout << shuffle[i].vec_index  <<  "\n";
																		}
															}
															else
															{
																		for(size_t i = 0; i < shuffle.size(); ++i)
																		{
																				cout << shuffle[i].vec_index <<  "\n" ;
																		}
															}
								}

  /*
	In the case only -n is given. We need to
	read one character at the time, convert the
	collection of characters into a string. Store it
  in our special vector. And after do the shuffling
	*/
					else
					{
													char c ;
													size_t myindex = 0 ;
													vector<char> V ;
													string s ;

						         /*
											Step 1: We read the characters one at the time
                     */
													while	((fread(&c,1,1,stdin)) != 0)
													{
																		if(c == '\n')
																		{
																					/*
																			     Step 2: Call our function so
																			     it can convert the collection
																			    of characters into a string
																			    */
																					characters_to_str(V,s);

																			   /*
																			    Step 3: Store it in our vector
																				*/
																					shuffle.push_back(vectype());
																					shuffle[myindex].vec_content	=	s;
																			    ++myindex ;
																					V.clear();
																		}
																		else
																		{
																					V.push_back(c);
																		}
													}

													/*
													Due to the structure and format of our while loop,
                          we need to store the last line which it
													was an empty line
													*/
												characters_to_str(V,s);
												shuffle.push_back(vectype());
												shuffle[myindex].vec_content	=	s;
												++myindex ;


													/*
													  Step 4: Do the shuffling
													 Same strategy as before.
													 Once we have a random number depending on the size
													of the vector we store it in a variable randnum
													which is a random selected index from the
											    vector. After that we just swap it with the current
													element of the loop which is i. It's very similar
													to the naive sorting algorithm but instead of choosing
													the smallest one, we just pick a random one.
													*/
													size_t randnum = 0;
													srand(time(0));

													for(size_t i = 0; i < shuffle.size(); ++i)
													{
															randnum = ( rand() % (shuffle.size()-i) + i)    ;
															swap(shuffle[i].vec_content,shuffle[randnum].vec_content);
													}

										      /*
													  Step 5: Print it out but be careful with the size.
													  If we are not careful we will get a segmentation error.
													*/

												if( count < shuffle.size()  )
												{
																	for(size_t i = 0; i < count ; ++i)
																	{
																		cout << shuffle[i].vec_content << "\n";
																	}
												}
												else
												{
																	for(size_t i = 0; i < shuffle.size(); ++i)
																	{
																			cout << shuffle[i].vec_content << "\n";
																	}
												}
				}

	return 0;
}