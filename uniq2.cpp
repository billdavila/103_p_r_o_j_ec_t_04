#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

using namespace std ;

#include<cstring>
#include<fstream>
#include<stdlib.h>


FILE* funiq =  0  ;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";


struct node{
	string line ;
	size_t counter ;
	node* next ;
};

string ufilename = "test_uniq.txt" ; // default filename


int main(int argc, char *argv[])
{
		// define long options
		static int showcount=0, dupsonly=0, uniqonly=0;
		static struct option long_opts[] = {
			{"count",         no_argument, 0, 'c'},
			{"repeated",      no_argument, 0, 'd'},
			{"unique",        no_argument, 0, 'u'},
			{"help",          no_argument, 0, 'h'},
			{0,0,0,0}
		};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;

			if(argc == 3)
			  ufilename = argv[2]  ;
				break;

			case 'd':
				dupsonly = 1;

			 if(argc == 3)
  			ufilename =  argv[2] ;


			break;


			case 'u':
				uniqonly = 1;

     if(argc == 3 )
 			ufilename = argv[2] ;

				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */

	if(showcount == 1 )
  {
				size_t count =  0  ;
				string myline ;
        node* L = NULL ;
				node* runner = L ;

       ifstream myfile ;
		   myfile.open( ufilename.c_str(), ios::in) ;

		 // FILE* f = fopen(ufilename.c_str() , "rb") ;

		  if(myfile.fail())
			{
				cout << "Couldn't open the file.\n" ;
				exit(1) ;
			}


			while(!myfile.eof())
			{
					getline(myfile, myline) ;

				  //if(!myline.empty() )
					//{
							if(L == NULL)
							{
								runner = new node ;
								runner->line = myline ;
								runner->counter = 1 ;
								runner->next = NULL ;
								L = runner ;
							}
							else
							{
								L->next = new node ;
								runner = L->next ;
								runner->counter = 1 ;
								runner->line = myline ;
								runner->next = NULL ;
							}
							if( L->line == runner->line )
							{
								++count ;
								L->counter = count ;
                      if(runner != L ) delete runner ;
						//		printf("%7lu " , count) ;
						//		cout << " " << L->line << "\n" ;
						}
						else
						{
							count = L->counter ;
							printf("%7lu " , count) ;
							cout << "" << L->line << "\n" ;
							count =  1 ;
						  //printf("%7lu " , count) ;
  						//cout << " " << runner->line << "\n";

							node* aux = L ;
							L = L->next  ;
							delete aux ;
						}
          //}
			 }
							if(!L->line.empty())
							{
									printf("%7lu " , count) ;
									cout << "" << L->line << "\n" ;
			            L = NULL ;
							}
              else
						{
			       --count ;
			       if (count > 0 )
						 {
							printf("%7lu " , count) ;
							cout << "" << L->line << "\n" ;
			        L = NULL ;
							}
						}
		 }

		 if(dupsonly == 1 )
		 {
							string myline , temp = "first" ;
              int counter = 0  ;

							ifstream myfile ;
							myfile.open( ufilename.c_str(), ios::in) ;
		  if(myfile.fail() )
			{
				cout << "Couldn't open the file.\n" ;
				exit(1) ;
			}

						while(!myfile.eof())
						{
									getline(myfile , myline) ;

						//	if( !myline.empty())
						  //{

								  if(temp == "first" )
									{
										temp = myline ;
                  }

								//	if(temp == myline) ++counter  ;

									if(temp != myline )
									{
														if(counter > 1)
														{
															cout << temp << "\n" ;
															counter =  0  ;
														}
										temp = myline ;
										counter = 0  ;
									}
                    ++counter ;


                 temp = myline ;

							//}

							}

							if(counter > 1 )
								 cout << temp << endl ;

			}


			if(uniqonly == 1 )
			{
						string myline , temp = "first" ;
						int counter = 0  ;

						ifstream myfile ;
						myfile.open( ufilename.c_str(), ios::in) ;

				if(myfile.fail() )
				{
						cout << "Couldn't open the file.\n" ;
						exit(1) ;
				}


						while(!myfile.eof())
						{
									getline(myfile , myline) ;

							//if( !myline.empty())
						  //{
								  if(temp == "first" )
									{
										temp = myline ;
										//++counter ;
                  }
									if(temp != myline )
									{
										   if(counter == 1 )
												 cout << temp << endl  ;

										temp = myline ;
										counter = 0  ;
									}
                 counter++ ;

                 temp = myline ;

							//}

							}

							if(counter == 1 && !myline.empty() )
								cout << temp << endl  ;
			}


	return 0;
}



