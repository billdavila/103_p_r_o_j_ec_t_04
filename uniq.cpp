#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

using namespace std ;

#include<cstring>
#include<stdlib.h>


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";


int main(int argc, char *argv[])
{
		// define long options
		static int showcount=0, dupsonly=0, uniqonly=0;
		static struct option long_opts[] = {
			{"count",         no_argument, 0, 'c'},
			{"repeated",      no_argument, 0, 'd'},
			{"unique",        no_argument, 0, 'u'},
			{"help",          no_argument, 0, 'h'},
			{0,0,0,0}
		};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
			break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */


	// Just like the actual uniq when
	// -c -d -u options are given or -d -u options
	// are given just read everyline
	if(showcount == 1 && dupsonly == 1 && uniqonly == 1 )
	{
		 string myline ;
		   while(getline(cin , myline) )
			 {

			 }
	}
	if(showcount == 0  &&  dupsonly == 1 && uniqonly == 1 )
	{
		string myline ;

		  while(getline(cin , myline) )
			{

			}

	}

  // the actual uniq print every line once
	if(showcount == 0 && dupsonly == 0 && uniqonly == 0 )
	{
		// The temporary variable will take
		// of the problem when the first line was an empty line
		string myline , temp = "________**" ;
		size_t counter = 0  ;

		 while(getline(cin , myline ) )
		 {
			  if(temp == "________**")
				{
					temp = myline ;
				}
				++counter ;

				if( temp != myline)
				{
					cout << temp << "\n" ;
					counter = 0 ;
				}

       temp = myline  ;
		 }
		 cout << temp << endl ;
	}


	// show the counter for every line, since
	// the input is sorted we only need to keep track of the line
	// we are reading and the previous line. Once they are different we
	// print the counter. Again we need to be careful with the first line we
	// read and the last one. Specially when we are dealing with spaces.
	if(showcount == 1 && dupsonly == 0 && uniqonly == 0 )
  {
				size_t count =  0  ;
				string myline, temp = "________**" ;

			while(getline(cin, myline) )
			{
							if(temp == "________**")
							{
								temp = myline  ;
							}
							if( temp == myline  )
							{
								++count ;
						   }
							else
						  {
								// the actual uniq print uses 7 spaces
								printf("%7lu " , count) ;
								cout << "" << temp << "\n" ;
							  count =  1 ;

						}
						temp = myline ;
			 }
			   // This will take care of the last line
			  // being an empty line
							if(!temp.empty())
							{
									printf("%7lu " , count) ;
									cout << "" << temp << "\n" ;
							}
              else
						  {
			          --count ;
			       if (count > 0 )
						 {
							printf("%7lu " , count) ;
							cout << "" << temp << "\n" ;
							}
						}
		 }

		 // This one will one print the lines that are duplicates
    // with their counter
		 if(dupsonly == 1 && uniqonly == 0 && showcount == 1 )
		 {
							string myline , temp = "________**" ;
              size_t counter = 0  ;

						while(getline(cin , myline) )
						{

								  if(temp == "________**" )
									{
										temp = myline ;
                  }

									if(temp != myline )
									{
										       // once we know that the counter is greater than one
                           // we know is a duplicate so we print it and also
										       // the counter

														if(counter > 1)
														{
															printf("%7lu " , counter) ;
															cout << temp << "\n" ;
															counter =  0  ;
														}
										temp = myline ;
										counter = 0  ;
									}
                    ++counter ;
                 temp = myline ;
							}

							// this will take care of the last line
							if(counter > 1 )
							{
								 printf("%7lu " , counter) ;
								 cout << temp << endl ;
							}
			}

			// this one prints only the duplicates
			// without their counter but same idea
			// as the previous one

			if(dupsonly == 1 && uniqonly == 0 && showcount == 0 )
		  {
							string myline , temp = "________**" ;
              int counter = 0  ;

						while(getline(cin , myline) )
						{
								  if(temp == "________**" )
									{
										temp = myline ;
                  }

									if(temp != myline )
									{
														if(counter > 1)
														{
															cout << temp << "\n" ;
															counter =  0  ;
														}
										temp = myline ;
										counter = 0  ;
									}
                    ++counter ;
                 temp = myline ;
							}

							if(counter > 1 )
								 cout << temp << endl ;

			}

			// this one only prints the uniq lines
			if(uniqonly == 1 && showcount == 0 && dupsonly == 0 )
			{
						string myline , temp = "________**" ;
						int counter = 0  ;
						while(getline(cin, myline))
						{
								  if(temp == "________**" )
									{
										temp = myline ;
                  }
									if(temp != myline )
									{
										   // if the counter is one then we know
										  // there is only one line and it's not
										 // a duplicate
										   if(counter == 1 )
												 cout << temp << endl  ;

										temp = myline ;
										counter = 0  ;
									}
                 ++counter ;

                 temp = myline ;
							}

							// this will take care of the last line
							if(counter == 1 )
								cout << temp << endl  ;
			}


			// This one prints only the uniq lines with
			// its counter which is going to be 1
			if(uniqonly == 1 && showcount == 1 && dupsonly == 0 )
			{
						string myline , temp = "________**" ;
						size_t counter = 0  ;
						while(getline(cin, myline))
						{
								  if(temp == "________**" )
									{
										temp = myline ;
                  }
									if(temp != myline )
									{
										   if(counter == 1 )
											 {
													printf("%7lu ", counter) ;
												 cout << temp << endl  ;
											 }
										temp = myline ;
										counter = 0  ;
									}
                 ++counter ;

                 temp = myline ;
							}

							// this will take care of the last line
							if(counter == 1 )
							{
								printf("%7lu ", counter) ;
								cout << temp << endl  ;
							}
			}
	return 0;
}


// passed all tests :D
