#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

void MinusE(vector<string> V1,int count){
	//declare variables
    int a,b,e,f,k,l,m;
    string c,d;
    m=(V1.size()/2)+1;
    string aa;
    string bb;
    //reverse vector
    for(int i=0;i<V1.size();i++){
        if(V1[i]==V1[V1.size()-i-1]){break;}//workes if size of vector is odd
        if(i==m){break;}
        aa = V1[i];
        bb = V1[V1.size()-i-1];
        V1[V1.size()-i-1] = aa;
        V1[i]=bb;
        }

        if(V1[V1.size()-1]=="-n"){
            V1[V1.size()-2]=V1[V1.size()-1]+V1[V1.size()];
            V1.pop_back();
        }
        //for(int i=0;i<V1.size();i++){cout<<"@ i="<<i<<" V1[i]="<<V1[i]<<"\n";}
        string imdone1 = V1[V1.size()-1];
        string imdone2 = V1[0];
        try{
        if(imdone1.at(1)=='n'&&imdone1.at(0)=='-')
            V1.pop_back();
    }catch(...){//cout<<"CRAPPPPP! \n";
        }

        try{
        if(imdone2.at(1)=='n'&&imdone2.at(0)=='-'){
            V1[V1.size()-1] = imdone2;
            V1[0] = imdone1;
            V1.pop_back();
        }
    }catch(...){//cout<<"CRAPPPPP! \n";
        }

    //Randomize our vector
    for(int i=0;i<V1.size();i++){
        for(int j=i;j<V1.size();j++){
            //WE DONT WANT TO LEAVE THIS LOOP UNTIL WE FIND A VALID REPLACEMENT FOR OUR VECTOR
            if(i!=j){
                a = rand()%V1.size();
                b = rand()%V1.size();
                c = V1[a];
                d = V1[b];
                V1[a] = d;
                V1[b] = c;
        }
    }
}
//print results
    for(size_t i =0;i<V1.size();i++){
        if(i==count){break;}
        cout<<V1[i]<<"\n";
    }
}


void MinusI(vector<string> V2,int count){
    //Algorithm

    //declare range variables  or data storage
    int lo,high;
    string hold;
    string hold2;
    int counter1;
    int counter2;
    string loT; //low number in text form
    string hiT; //high number in text form
    string A;
    int j,k;

    for(int i=0;i<V2.size();i++){hold.append(V2[i]);}

    //for(int i=0;i<hold.size();i++){cout<<hold[i];} check point
    for(int i=0;i<hold.size();i++){
        if(hold[i]=='i'){
            hold[i] = ' ';
            counter1 = i+1;
        }
        else if(hold[i]=='-'){hold[i] = ' ';}
        else if(hold[i]=='n'){
            counter2=i-2;
            break;
            }
        else if(i==(hold.size()-1)){counter2=i;}
    }
    //cout<<"\n";
    //cout<<"hold is: "<<hold<<" hold.size()="<<hold.size()<<"\n";
    //cout<<"counter1 is: "<<counter1<<" counter2 is: "<<counter2<<"\n";
    //for(int i=0;i<hold.size();i++){cout<<"@ i="<<i<<" hold[i]= "<<hold[i]<<"\n";}
    //separate to for loops to sum up to the length of the string
    string B;
    for(int i=counter1;i<=counter2;i++){
        B=hold.at(i);
        hold2.append(B);
        }
    int ccc;
    ccc = hold2.find(" ");
    //cout<<"hold2 is: "<<hold2<<" AND hold2.size() is: "<<hold2.size()<<" and @ i=0 hold2.at(i)= "<<hold2.at(0)<<"\n";
    int T = 0;
    for(int i=0;i<hold2.size();i++){
        B=hold2.at(i);
        //cout<<"@ i ="<<i<<" B= "<<B;
        if(i==ccc){
            j=i;
            break;
        }
        loT.append(B);
    }
    //cout<<"loT is: "<<loT<<"\n";
    int zyx;
    //this will force stoi to work (using try and catch makes it so that your code will ALWAYS RUN and impossible for it not to if used properly
    try{
        lo = stoi(loT);
    }catch(...){
        zyx = 1;
        lo = zyx;
        T=1;
        //cout<<"setting lo equal to zyx which is: "<<lo<<"\n";
    }
    //cout<<"i've escaped \n";
    for(int i=j+1;i<(hold2.size());i++){
        B=hold2.at(i);
        if(T==1){
            hiT = "0";
            break;
            }
        hiT.append(B);
    }
    //cout<<"hiT is: "<<hiT<<"\n";
    int zyxx;
    try{
        high = stoi(hiT);
    }catch(...){
        zyxx = 0;
        high = zyxx;
        //cout<<"setting high equal to: "<<high<<"\n";
    }

    //step 2: Create list of numbers to permute
    vector<int> W;
    for(int i=lo;i<=high;i++){
        if(lo>high){break;}
        else if(lo<0){break;}
        W.push_back(i);
        }

    int a,b,c,d;
    for(int i=0;i<W.size();i++){
        if(lo>high){break;}
        else if(lo<0){break;}
        for(int j=i;j<W.size();j++){
            //WE DONT WANT TO LEAVE THIS LOOP UNTIL WE FIND A VALID REPLACEMENT FOR OUR VECTOR
            if(i!=j){
            a = rand()%W.size();
            b = rand()%W.size();
            c = W[a];
            d = W[b];
            W[a] = d;
            W[b] = c;
            }
        }
    }
    //step 3: perform a random permutation

    //cout<<"\n";
    //cout<<"n is: "<<n<<"\n";
    for(int i=0;i<W.size();i++){
        if(lo>high){break;}
        else if(lo<0){break;}
        else if(i==count){break;}
        cout<<W[i]<<"\n";
    }
    //cout<<"stop is "<<STOP;
}

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */
  //getting our function ready
    vector<string> V1; // 1st vector to do -e
    vector<string> V2; // 2nd vector to do -i
    vector<string> V3; // reverse of V1
    for(int i=2;i<argc;i++){V1.push_back(argv[i]);}
    for(int i=1;i<argc;i++){V2.push_back(argv[i]);}
    string dontcare;
    string doesntmatter;
    doesntmatter=V2[0];
    dontcare=doesntmatter.at(1);
    //cout<<"\n";
    //cout<<"dontcare is: "<<dontcare<<"\n";
    //cout<<"\n"<<"count is: "<<count;
    if(echo!=1){MinusI(V2,count);}
    else if(echo==1){MinusE(V1,count);}

	return 0;
}
