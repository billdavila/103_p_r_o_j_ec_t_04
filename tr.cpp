/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *  Tutor: Songren Zhao
 *  www.cplusplus.com
 *  Ai Hua Li , she was part of my group and she was really helpful
 *  stackoverflow.com
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <map>
using std::map;
using namespace std;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";



void escape(string& s)
{
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */

     /*
	    This will take care of the invalid sequences, and
	   also consider the main cases /n , /t , //. There are
	  more valid sequences which can be done by putting more
	  if statements but this will do it for now.
	  */
			for (size_t i = 0 ; i < s.length() ; ++i)
	    {
				   if( s[i] == '\\' && s[i+1] == 'n')
           {
							s[i+1] = '\n' ;
						  s.erase(s.begin() +i ) ;
					 }
           else if(s[i] == '\\' && s[i+1] == 't')
           {
						   s[i+1] = '\t' ;
						   s.erase(s.begin() +i ) ;
					 }
					 else if(s[i] == '\\' && s[i+1] == '\\')
					 {
						 s[i+1] = '\\' ;
						 s.erase(s.begin() + i ) ;
					 }

					 if(s[i] == '\\' && s[i+1] != 'n' && s[i +1] != 't' && s[i+1] != '\\')
					 {
					     s.erase(s.begin() +i ) ;
					 }
			}
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if (del)
	{
	    	if (optind != argc-1)
				{
			      fprintf(stderr, "wrong number of arguments.\n");
			      return 1;
				}
	}
	else if (optind != argc-2)
	{
	     	fprintf(stderr, "Exactly two strings must be given when translating.\n");
		   return 1;
	}



	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";


	/* process any escape characters: */
	escape(s1);
	escape(s2);


//	cout << s1 << endl ; just checking if the escape function was actually working.

	char ch  ;

  if(del == 1 )
	{
		/* it was a lot easier to identify when to print
		  than actually deleting that specific character */
		 // this one is when we only need to delete
		// the charcaters found on the first string
   // from the given input
		   if(comp == 0)
			 {
				  while(fread(&ch, 1 ,1 , stdin ) )
					{
						 // this means we did not find it
						 // so we print the character
						// if we don't find in the first string
						// then we don't print it. Instead of modifying it
						// directly
						   if(s1.find(ch, 0)  == string::npos )
							 {
								 cout << ch ;
							 }
					}
			 }
			 else
			 {
				  // the actual tr does not print anything
				  // so we do not print anything when -d and
				  // -c options are given
				   while(fread(&ch , 1 ,1 ,stdin ) )
					 {

					 }
			 }
	}

	// if no options are given then we need to do some
	// mapping with the first and second string given
	if (comp == 0 && del == 0 )
  {
            map<char,char> Map;
            map<char,char>::iterator it;
        if (s1.length() > s2.length())
        {
					/* if the first string is larger then we need to first
					 map the one that we can map. In respect of their indexes. Once
					 it runs out, the rest of the characters of the first string
					will be mapped to the last character of the second string
					*/
               size_t i;
              for (i = 0; i < s2.length(); ++i)
                  Map.insert(pair<char,char>(s1[i],s2[i]));

              for ( size_t j = i; j < s1.length() ; ++j)
                  Map.insert(pair<char,char>(s1[j],s2[i-1]));
        }
        else
        {
					 /*
					  if the second string is larger then we do not
					   need to worry since every character from the first string
					  will be mapped with someone from the second string and once
					  is full it will ignore the rest.
					*/
                for (size_t i = 0 ; i < s1.length() ; ++i)
                  Map.insert(pair<char,char>(s1[i],s2[i]));
        }
          while ( fread(&ch,1,1,stdin) )
          {
						  /*if the character appears in the map, we do the change in
						   translation and print its correspending value else we
						   just print the character.
						*/
                  it = Map.find(ch);

						    // this means you actually found it so
						   // we need to do the translation
						   // the else is you did not find it on our map
						  // so just print the charcater that you were reading
					 if (Map.find(ch) != Map.end())
                      cout << (*it).second;
                     else
                      cout << ch;
		      }
	}


	/*
	This one is when only the -c option is given.
	we need to map the given string with the index of
	the second string while taking in consideration that we
	are going to have some holes given by the first string
	The first string character will be ignore and then print it
	*/
  if (comp == 1 && del == 0 )
  {
	     	vector<char> table  ;
		    size_t i;

		    /*
		      Fill up the table with every character from
		      the second string
		    */
         for (i = 0; i < s2.length(); ++i)
			        table.push_back(s2[i]);

		        size_t j = i ;
		   /*
		   Once you run out of character of the second string
		  , just fill up with the last character of the second
		   string.
		  */
        while (j < 256)
        {
            table.push_back(s2[i-1]);
            ++j;
        }

	       /*
          Once we have the table, we can do the translation,
          we just need to deal with the holes.
         */

        while (fread(&ch,1,1,stdin))
        {

							/*
					     if you find it in the first string ,
					     then just print the same character.
					   */
              if (s1.find(ch,0) != string::npos)
                  cout << ch ;
              else
              {
								/*
								   if you did not find it then we need to find
								 its modified translation from the table by
								counting the holes and then subtracting from
								 the index in the table.
								*/
                  int holes = 0 ;
                  for (size_t i = 0; i < s1.length(); ++i)
                  {
                      if ((int)s1[i] < (int)ch )
                        ++holes;
                  }
                  cout << table[(int)ch - holes];
              }
          }
	}


	return 0;

}
